package com.example.android.justjava;

import android.content.Intent;
import android.icu.text.NumberFormat;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    int quantity = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        EditText costumerName = (EditText) findViewById(R.id.costumer_name);
        String costumerNameOut = costumerName.getText().toString();

        CheckBox whippedCreamCheckBox = (CheckBox) findViewById(R.id.whipped_cream_checkbox);
        boolean hasWhippedCream = whippedCreamCheckBox.isChecked();

        CheckBox ChocolateCheckBox = (CheckBox) findViewById(R.id.chocolate_checkbox);
        boolean hasChocolate = ChocolateCheckBox.isChecked();
//        Log.v("MainActivity", "Has whipped cream: " + hasWhippedCream);
        int price = calculatePrice(hasWhippedCream, hasChocolate);

        String outPutMessage = createOrderSummary(price, hasWhippedCream, hasChocolate, costumerNameOut);

        displayMessage(outPutMessage);

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"angotheb@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Just Java order for " + costumerNameOut);
        intent.putExtra(Intent.EXTRA_TEXT, outPutMessage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }

    /**
     * This method calulate the total price of cofees ordered
     *
     * @param addwhippedCream is whether or not the user wants whipped cream topping
     * @param addchocolate    is whether or not the user wants chocolate topping
     * @return total price
     */
    public int calculatePrice(boolean addwhippedCream, boolean addchocolate) {

        int basePrice = 5;

        if (addwhippedCream) {
            basePrice = basePrice + 1;
        }
        if (addchocolate) {
            basePrice = basePrice + 2;
        }
        return quantity * basePrice;
    }

    /**
     * This method  must make the summary of the order
     *
     * @param price           of order
     * @param addWhippedCream is whether or not the user wants whipped cream topping
     * @param addChocolate    is whether or not the user wants chocolate topping
     * @param costumerName    is name of the costumer
     * @return text summary
     */
    public String createOrderSummary
    (int price, boolean addWhippedCream, boolean addChocolate, String costumerName) {
        String priceMessage = getString(R.string.name)+": " + costumerName;
        priceMessage += "\n" + getString(R.string.add)+ getString(R.string.whipped_cream)+": " + addWhippedCream;
        priceMessage += "\n" + getString(R.string.add)+ getString(R.string.chocolate)+": " + addChocolate;
        priceMessage += "\n" + getString(R.string.quantity)+": " + quantity;
        priceMessage += "\n" + getString(R.string.total)+ price;
        priceMessage += "\n" + getString(R.string.thank_message);

        return priceMessage;

    }

    /**
     * This method displays the given text on the screen.
     */
    private void displayMessage(String message) {
        TextView orderSummaryTextView = (TextView) findViewById(R.id.order_summary_text_view);
        orderSummaryTextView.setText(message);
    }

    public void decrement(View view) {
        if (quantity == 1) {
            Toast.makeText(this, "You cannot have less than 1 coffee", Toast.LENGTH_SHORT).show();
            return;
        }
        quantity = quantity - 1;
        displayQuantity(quantity);
    }

    public void increment(View view) {
        if (quantity == 100) {
            Toast.makeText(this, "You cannot have more than 100 coffees", Toast.LENGTH_SHORT).show();
            return;
        }
        quantity = quantity + 1;
        displayQuantity(quantity);
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayQuantity(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

    /**
     * This method displays the given price on the screen.
     */
    private void displayPrice(int number) {
        TextView priceTextView = (TextView) findViewById(R.id.order_summary_text_view);
        priceTextView.setText(NumberFormat.getCurrencyInstance().format(number));
    }
}